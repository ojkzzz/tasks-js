// Оцениваю задачу на 5 из 10
// Предварительная оценка трудозатрат: 20 минут на написание функции и 30 минут на написание тестов + ручное тестирование.
// Написание функции составило 20 минут + написание тестов и ручное тестирование заняло так же 20 минут.
// Сложность алгоритма составляет О(n)

//В каждый цикл добавил проверку на истечение максимальной длительности на выполнение данного алгоритма.

const timeoutHandler = (start, timeRange = 5000) => {
  const now = new Date().getTime();
  if (now - start >= timeRange) throw new Error("Too long process");
};

const getPercentEquality = (data) => {
  const start = new Date().getTime();

  //Проверяем на тип входных данных
  if (!Array.isArray(data)) throw new Error("Invalide type of data");

  //Наполнение массива числами и проверка каждого элемента на наличие не валидных входных данных
  const numericData = data.map((el) => {
    const num = parseFloat(el);
    if (isNaN(num)) throw new Error("Some of the items are not valid numbers");
    timeoutHandler(start);
    return num;
  });

  //Находим общую сумму входных данных
  const sumOfSlices = numericData.reduce((acc, cur) => {
    timeoutHandler(start);
    return acc + cur;
  }, 0);

  //Проверяем сумму входных данных
  if (sumOfSlices === 0) throw new Error("Sum of slices should not be zero");

  //Вычисляем проценты и наполняем результирующий массив
  const percentages = numericData.map((int) => {
    timeoutHandler(start);
    return ((int / sumOfSlices) * 100).toFixed(3);
  });
  return percentages;
};

const arr = new Array(10000).fill("1.5");
const data = getPercentEquality(arr);
console.log(data);

module.exports = getPercentEquality;

getPercentEquality([1, 2, 3]);
