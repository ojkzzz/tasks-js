const getPercentEquality = require("./building");

describe("Тестирование getPercentEquality", () => {
  test("Тест валидных строк", () => {
    expect(getPercentEquality(["1.5", "3", "6", "1.5"])).toEqual([
      "12.500",
      "25.000",
      "50.000",
      "12.500",
    ]);
  });
  test("Тест валидных строк + чисел", () => {
    expect(getPercentEquality([1.5, "3", 6, 1.5])).toEqual([
      "12.500",
      "25.000",
      "50.000",
      "12.500",
    ]);
  });
  test("Тест не валидной структуры", () => {
    expect(() =>
      getPercentEquality({ name: "Oleg", age: 28, job: "Front-end", grade: "middle+" })
    ).toThrowError();
  });
  test("Тест не валидных элементов в валидной структуре", () => {
    expect(() =>
      getPercentEquality([
        "1.5",
        "3",
        "6",
        "1.5",
        { name: "Oleg", age: 28, job: "Front-end", grade: "middle+" },
      ])
    ).toThrowError();
  });
});
