//Оцениваю задачу на 5 из 10
//Предварительная оценка трудозатрат: 20 минут на написание функции и 30 минут на написание тестов + ручное тестирование.
//Написание функции составило 20 минут + написание тестов и ручное тестирование заняло так же 20 минут.
//Сложность будет зависить от входящей функции и скорее всего будет -  О(f(n))
//Расходуемая память будет иметь затраты O(n), где n - количество аргументов в передаваемой функции

const cacheFunc = (func) => {
  const cacheTable = {};
  return (...args) => {
    const key = JSON.stringify(args);
    if (!cacheTable[key]) {
      cacheTable[key] = Promise.resolve(func(...args)).then((response) => {
        //проверяю на то, является ли response инстансом Response
        //при необходимости привожу к json-y
        if (
          response &&
          typeof response === "object" &&
          "json" in response &&
          typeof response.json === "function"
        ) {
          return response.json();
        } else {
          return response;
        }
      });
    }
    return cacheTable[key];
  };
};

module.exports = cacheFunc;
