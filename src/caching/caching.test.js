const cacheFunc = require("./caching");

describe("cacheFunc", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  test("Кэширование данных", async () => {
    const mockFunc = jest.fn(() => {
      return { result: "mocked data" }; // Заглушка для функции
    });

    const cachedFunction = cacheFunc(mockFunc);

    // Первый вызов функции
    let data = await cachedFunction({ param: "value" });

    // Проверяем, что функция была вызвана один раз
    expect(mockFunc).toHaveBeenCalledTimes(1);
    // Проверяем возвращенные данные
    expect(data).toEqual({ result: "mocked data" });

    // Второй вызов с теми же параметрами
    data = await cachedFunction({ param: "value" });

    // Проверяем, что функция не была вызвана снова
    expect(mockFunc).toHaveBeenCalledTimes(1);
    // Проверяем, что данные были взяты из кэша
    expect(data).toEqual({ result: "mocked data" });
  });

  test("Корректное кэширование на основе параметров", async () => {
    const mockFunc = jest.fn(async () => {
      return { result: "mocked data" }; // Заглушка для функции
    });

    const cachedFunction = cacheFunc(mockFunc);

    // Вызываем функцию с разными параметрами
    await cachedFunction({ param: "value1" });
    await cachedFunction({ param: "value2" });

    // Проверяем, что функция была вызвана дважды (для каждого набора параметров)
    expect(mockFunc).toHaveBeenCalledTimes(2);
  });
});
